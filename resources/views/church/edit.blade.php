@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-5">
                <div class="card-header text-white" style="background-color:#4f68dc">Edit Church
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>
                </div>

                <div class="card-body">
                    @if (session('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_message') }}
                        </div>
                    @endif

                    <form method="post" action="{{route($route.'.update',$$model->id)}}">
                        @csrf
                        <div class="form-group">
                            <label>Nama Church</label>
                            <input type="text" class="form-control" name="name" value="{{$$model->name}}" placeholder="Isi Nama Church" />
                        </div>
                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>


@endsection

