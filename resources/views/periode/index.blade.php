@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">List Periode</div>

                <div class="card-body">
                    @if (session('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_message') }}
                        </div>
                    @endif
                    @if (session('error_message'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error_message') }}
                        </div>
                    @endif
                    <a href="{{route($route.'.create')}}" class="btn btn-primary mb-2">Create</a>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Tanggal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($$model as $data)
                            <tr>
                                <td>{{$data->name}}</td>
                                <td>{{$data->tanggal}}</td>
                                <td>
                                    <a href="{{route($route.'.edit',$data->id)}}" class="btn btn-warning">Edit</a>
                                    <a href="{{route($route.'.delete',$data->id)}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
