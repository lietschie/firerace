@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-header text-white" style="background-color:#4f68dc">Edit Cool
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>

                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="post" action="{{route($route.'.update',$$model->id)}}">
                        @csrf
                        <div class="form-group">
                            <label>Nama Cool</label>
                            <input type="text" class="form-control" name="name" value="{{$$model->name}}"
                                placeholder="Isi Nama Cool" />
                        </div>
                        <p>
                            Total Point : {{$$model->total_point}}
                        </p>

                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">List Score Per Periode

                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <?php
                                    $totalSemuaMinggu = 0;
                                    $columnSpan = 0;
                                    $listScorePeriode = $$model->hasManyScore()->orderBy('id','desc')->get()->groupBy('periode_id');
                                ?>
                                @foreach($listScorePeriode as $group)
                                <?php
                                    $columnSpan = (count($group) * 2) + 1;
                                ?>

                                @if($loop->first)
                                <tr>
                                    <td rowspan="2">Periode</td>
                                    @foreach($group as $td)
                                    <td colspan="2">{{$td->parameter->name}}</td>
                                    @endforeach
                                    <td rowspan="2">Score Mingguan</td>
                                </tr>
                                @endif
                                @if($loop->first)
                                <tr>
                                    @foreach($group as $column)
                                    <td>Qty</td>
                                    <td>Total Score</td>
                                    @endforeach
                                </tr>
                                @endif
                                <tr>
                                    <td>
                                        {{$group[0]->periode->name}}

                                    </td>
                                    @foreach($group as $score)
                                    <td>{{$score->qty}}</td>
                                    <td>{{$score->total_score}}</td>
                                    @endforeach
                                    <?php 
                                        $sumPeriode = $group->sum('total_score');
                                    ?>
                                    <td>{{$sumPeriode}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="{{$columnSpan}}">Total Semuanya :</td>
                                    <td >{{$$model->total_point}}</td>
                                </tr>

                               

                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
