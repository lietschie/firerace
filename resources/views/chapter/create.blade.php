@extends('layouts.app')

@push('css-plugins')
<link  href="{{asset('vendor/select2/dist/css/select2.min.css')}}" rel="stylesheet">
<link  href="{{asset('vendor/select2-bootstrap/dist/select2-bootstrap.min.css')}}" rel="stylesheet">
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">Tambah Chapter 
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="{{route($route.'.store')}}">
                        @csrf
                        <div class="form-group">
                            <label>PIC</label>
                            <select class="form-control select2" name="pic_id">
                                @foreach($listUsers as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Chapter</label>
                            <input type="text" class="form-control" name="name" placeholder="Isi Nama Chapter" />
                        </div>
                       

                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-plugins')
<script src="{{asset('vendor/select2/dist/js/select2.full.min.js')}}"></script>
@endpush

@push('js-scripts')
<script>
        $(document).ready(function(){
            $('.select2').select2({
                theme :"bootstrap"
            });
        });
</script>
@endpush
