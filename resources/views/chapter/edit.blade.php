@extends('layouts.app')

@push('css-plugins')
<link  href="{{asset('vendor/select2/dist/css/select2.min.css')}}" rel="stylesheet">
<link  href="{{asset('vendor/select2-bootstrap/dist/select2-bootstrap.min.css')}}" rel="stylesheet">
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-5">
                <div class="card-header text-white" style="background-color:#4f68dc">Edit Chapter
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>

                </div>

                <div class="card-body">
                    @if (session('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_message') }}
                        </div>
                    @endif

                    <form method="post" action="{{route($route.'.update',$$model->id)}}">
                        @csrf
                        <div class="form-group">
                            <label>PIC</label>
                            <select class="form-control select2" name="pic_id">
                                @foreach($listUsers as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Chapter</label>
                            <input type="text" class="form-control" name="name" value="{{$$model->name}}" placeholder="Isi Nama Chapter" />
                        </div>
                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">
                    Daftar <strong>{{$$model->name ?? ''}}</strong> Cool
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="post" action="{{route($route.'.attach-anggota',$$model->id)}}">
                        @csrf
                        <div class="form-group">
                            <label>Cool</label>
                            <select class="form-control select2" name="cool_id">
                                @foreach($listCool as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Insert Cool">
                    </form>
                    <div class="table-responsive">
                        <table class="table table-bordered mt-2">
                            <tr>
                                <th>Cool</th>
                                <th>Leader</th>
                                <th>Leader Phone</th>
                                <th>Lokasi</th>
                                <th>Action</th>
                            </tr>
                            @foreach($$model->pivotCool as $cool)
                            <tr>
                                <td>{{$cool->name}}</td>
                                <td>{{$cool->leader ? $cool->leader->name : '' }}</td>
                                <td>{{$cool->leader ? $cool->leader->no_hp : '' }}</td>
                                <td>{{$cool->location}}</td>
                                <td>
                                    <a href="{{Route($route.'.detach-anggota',[$$model->id,$cool->id])}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach

                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@push('js-plugins')
<script src="{{asset('vendor/select2/dist/js/select2.full.min.js')}}"></script>
@endpush

@push('js-scripts')
<script>
        $(document).ready(function(){
            $('.select2').select2({
                theme :"bootstrap"
            });
        });
</script>
@endpush

