@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header text-white font-weight-bold" style="background-color:#4f68dc">
          List Score Cool : {{$$model->name}} ,Periode {{$periode->name}}

        </div>

        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          <table class="table table-bordered">
            <thead>
              <tr>
                <td>Parameter</td>
                <td>Score</td>
                <td>Qty</td>
                <td>Total Score</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>
              @foreach($cool->hasManyScore as $score)
              <tr>
                <td>{{$score->parameter->name}}</td>
                <td>{{$score->score}}</td>
                <td>{{$score->qty}}</td>
                <td>{{$score->total_score}}</td>
                <td>
                  <a href="{{route($route.'.detail-periode-scoredelete',[$$model->id,$periode->id,$score->id])}}" class="btn btn-danger">
                    Delete
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
         
        </div>
      </div>
    </div>
  </div>
</div>


@endsection