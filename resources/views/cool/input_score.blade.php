@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">Input Score
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="post" action="{{route($route.'.store-score')}}">
                        @csrf
                        <div class="form-group">
                            <label>Cool</label>
                            <select class="form-control" name="cool_id">
                                @foreach($listCool as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                            <!-- <input type="text" class="form-control" name="name" placeholder="Isi Nama Cool" /> -->
                        </div>
                        <div class="form-group">
                            <label>Periode</label>
                            <select class="form-control" name="periode_id">
                                @foreach($listPeriode as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <section>
                            @foreach($listParameter as $key => $param)
                                @if($param->type == 'check')
                                <div class="form-group form-check">
                                    <input class="form-check-input" name="parameter[{{$param->id}}][qty]" type="checkbox"
                                        value="1" id="defaultCheck{{$key}}">
                                    <input type="hidden" name="parameter[{{$param->id}}][type]" value="{{$param->type}}" />
                                    <input type="hidden" name="parameter[{{$param->id}}][score]"
                                        value="{{$param->score}}" />
                                    <label class="form-check-label" for="defaultCheck{{$key}}">
                                        {{$param->name}}
                                    </label>
                                </div>
                                @else
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">{{$param->name}}</label>
                                    <div class="col-md-2">
                                        <input type="hidden" name="parameter[{{$param->id}}][type]" value="{{$param->type}}" />
                                        <input type="number" name="parameter[{{$param->id}}][qty]" value="0" class="form-control" />
                                        <input type="hidden" name="parameter[{{$param->id}}][score]"
                                        value="{{$param->score}}" />
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        </section>




                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
