@extends('layouts.app')

@push('css-plugins')
<link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}" />
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">List Cool</div>

                <div class="card-body">
                    @if(session('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_message') }}
                        </div>
                    @endif
                    @if(session('error_message'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error_message') }}
                        </div>
                    @endif
                    <a href="{{route($route.'.generate-score')}}" class="btn btn-dark">Generate Score</a>
                    <a href="{{route($route.'.create')}}" class="btn btn-primary">Create</a>
                    <table class="table table-bordered mt-2" id="example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Leader</th>
                                <th>Leader Phone</th>
                                <th>Lokasi</th>
                                <th>Total Point</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($$model as $data)
                            <tr>
                                <td>{{$data->name}}</td>
                                <td>{{$data->leader ? $data->leader->name : ""}}</td>
                                <td>{{$data->leader ? $data->leader->no_hp : ""}}</td>
                                <td>{{$data->location}}</td>
                                <td>{{$data->total_point}}</td>
                                <td>
                                    <a href="{{route($route.'.edit',$data->id)}}" class="btn btn-warning">Edit</a>
                                    <a href="{{route($route.'.detail',$data->id)}}" class="btn btn-info">Detail</a>
                                    <a href="{{route($route.'.delete',$data->id)}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-plugins')
    <script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}" ></script>
    <script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}" ></script>
@endpush
@push('js-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable({ "sPaginationType": "full_numbers" });
            // $('#example').DataTable();
        });
    </script>
@endpush
