@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-5">
                <div class="card-header text-white" style="background-color:#4f68dc">Edit Cool
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>

                </div>

                <div class="card-body">
                    @if (session('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_message') }}
                        </div>
                    @endif

                    <form method="post" action="{{route($route.'.update',$$model->id)}}">
                        @csrf
                        
                        <div class="form-group">
                            <label>Nama Cool</label>
                            <input type="text" class="form-control" name="name" value="{{$$model->name}}" placeholder="Isi Nama Cool" />
                        </div>
                        <div class="form-group">
                            <label>Leader</label>
                            <select name="leader_id" class="form-control">
                                @foreach($leaders as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Location</label>
                            <input type="text" class="form-control" value="{{$$model->location}}" name="location"/>
                        </div>
                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">
                    Daftar Anggota
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="post" action="{{route($route.'.attach-anggota',$$model->id)}}">
                        @csrf
                        <div class="form-group">
                            <label>Anggota</label>
                            <select class="form-control" name="user_id">
                                @foreach($listUser as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Insert Anggota">
                    </form>
                    <table class="table table-bordered mt-2">
                        <tr>
                            <th>User</th>
                            <th>Action</th>
                        </tr>
                        @foreach($$model->pivotUser as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>
                                <a href="{{Route($route.'.detach-anggota',[$$model->id,$user->id])}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

