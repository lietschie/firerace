@extends('layouts.app')
@push('css-plugins')
<link  href="{{asset('vendor/select2/dist/css/select2.min.css')}}" rel="stylesheet">
<link  href="{{asset('vendor/select2-bootstrap/dist/select2-bootstrap.min.css')}}" rel="stylesheet">
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">Create Cool
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="{{route($route.'.update',$$model->id)}}">
                        @csrf
                        <div class="form-group">
                            <label class="font-weight-bold">Nama User</label>
                            <input type="text" class="form-control" required value="{{$$model->name}}" name="name" placeholder="Isi Isi Nama" />
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Email</label>
                            <input type="email" class="form-control" name="email" value="{{$$model->email}}" placeholder="Ini Isi Email" />
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">No Hp</label>
                            <input type="text" class="form-control" name="no_hp" value="{{$$model->no_hp}}" placeholder="Ini Isi No Hp" />
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Alamat</label>
                            <input type="text" class="form-control" name="address" placeholder="Ini Isi Alamat" />
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Ini Password" />
                        </div>
                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-plugins')
<script src="{{asset('vendor/select2/dist/js/select2.full.min.js')}}"></script>
@endpush

@push('js-scripts')
<script>
        $(document).ready(function(){
            $('.select2').select2({
                theme :"bootstrap"
            });
        });
</script>
@endpush
