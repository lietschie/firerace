@extends('layouts.app')

@push('css-plugins')
<link rel="stylesheet" href="{{asset('vendor/datatables/css/dataTables.bootstrap4.min.css')}}" />
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">List User</div>

                <div class="card-body">
                    @if (session('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_message') }}
                        </div>
                    @endif
                    @if (session('error_message'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error_message') }}
                        </div>
                    @endif
                    <a href="{{route($route.'.create')}}" class="btn btn-primary mb-2">Create</a>
                    <table class="table table-bordered table-striped" style="width:100%" id="example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>No Hp</th>
                                <th>Alamat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($$model as $data)
                            <tr>
                                <td>{{$data->name}}</td>
                                <td>{{$data->email}}</td>
                                <td>{{$data->no_hp}}</td>
                                <td>{{$data->address}}</td>
                                <td>
                                    <a href="{{route($route.'.edit',$data->id)}}" class="btn btn-warning">Edit</a>
                                    <a href="{{route($route.'.delete',$data->id)}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>No Hp</th>
                                <th>Alamat</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-plugins')
    <script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}" ></script>
    <script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}" ></script>
@endpush
@push('js-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable({ "sPaginationType": "full_numbers" });
            // $('#example').DataTable();
        });
    </script>
@endpush
