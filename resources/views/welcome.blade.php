@extends('layouts.app')

@section('content')
    <section class="justify-content-center mb-5" style="margin-top : -1.5rem;background-color:#6c45ba">
        
        <img src="{{asset('img/banner-firerace.jpg')}}" class="img-fluid mx-auto d-block" />
    </section>
    <div class="container">
        <div class="row ">
            <div class="col-md-6">

                

                <div class="card">
                    <div class="card-header text-white" style="background-color:#4f68dc">
                        Rank
                    </div>
                    <div class="card-body">

                        <table class="table ">
                            <tr>
                                <th>#</th>
                                <th>Cool</th>
                                <th>Total Point</th>
                            </tr>
                            @foreach($listCool as $key =>  $data)
                            <tr>
                                <td width="10%">{{$key+1}}</td>
                                <td width="50%">{{$data->name}}</td>
                                <td width="40%">{{$data->total_point}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection