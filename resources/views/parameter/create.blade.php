@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">Create Parameter
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="{{route($route.'.store')}}">
                        @csrf
                        <div class="form-group">
                            <label>Nama Parameter</label>
                            <input type="text" class="form-control" name="name" placeholder="Isi Nama Parameter" />
                        </div>
                        <div class="form-group">
                            <label>Score</label>
                            <input type="number" class="form-control" value="0" name="score" placeholder="Isi Default Score" />
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <select class="form-control" name="type">
                                <option value="check">check</option>
                                <option value="value">value</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        
                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
