@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color:#4f68dc">Edit Parameter
                    <a href="{{route($route.'.index')}}" class="btn btn-dark float-right">Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="{{route($route.'.update',$$model->id)}}">
                        @csrf
                        <div class="form-group">
                            <label>Nama Parameter</label>
                            <input type="text" class="form-control" value="{{$$model->name}}" name="name" placeholder="Isi Nama Parameter" />
                        </div>
                        <div class="form-group">
                            <label>Score</label>
                            <input type="number" class="form-control" value="{{$$model->score}}"  name="score" placeholder="Isi Default Score" />
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <select class="form-control" name="type">
                                <option value="check" {{$$model->type == 'check' ? 'selected' : ''}}>check</option>
                                <option value="value" {{$$model->type == 'value' ? 'selected' : ''}}>value</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3">{{$$model->description}}</textarea>
                        </div>
                        <input type="submit" class="btn btn-primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
