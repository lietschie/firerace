<?php

use Illuminate\Database\Seeder;
use App\User;
class CoolLeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::updateOrCreate([
            'id' => 5
        ],[
            'id' => 5,
            'name' => 'Zakhius',
            'email' => 'zakhius@elroichurch.com',
            'password' => Hash::make('password_zakhius'),
            'no_hp' => '087775658511',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 6
        ],[
            'id' => 6,
            'name' => 'Benny.M',
            'email' => 'benny@elroichurch.com',
            'password' => Hash::make('password_benny'),
            'no_hp' => '081295866773',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 7
        ],[
            'id' => 7,
            'name' => 'marie',
            'email' => 'marie@elroichurch.com',
            'password' => Hash::make('password_marie'),
            'no_hp' => '081295866773',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 8
        ],[
            'id' => 8,
            'name' => 'tati',
            'email' => 'tati@elroichurch.com',
            'password' => Hash::make('password_tati'),
            'no_hp' => '082113071010',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 9
        ],[
            'id' => 9,
            'name' => 'tabrani',
            'email' => 'tabrani@elroichurch.com',
            'password' => Hash::make('password_tabrani'),
            'no_hp' => '085810178612',
            'role' => 'Cool Leader'
        ]);
        //Chapter 2
        User::updateOrCreate([
            'id' => 10
        ],[
            'id' => 10,
            'name' => 'wijaya',
            'email' => 'wijaya@elroichurch.com',
            'password' => Hash::make('password_wijaya'),
            'no_hp' => '089505938266',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 11
        ],[
            'id' => 11,
            'name' => 'eni',
            'email' => 'eni@elroichurch.com',
            'password' => Hash::make('password_eni'),
            'no_hp' => '08569098282',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 12
        ],[
            'id' => 12,
            'name' => 'risme',
            'email' => 'risme@elroichurch.com',
            'password' => Hash::make('password_risme'),
            'no_hp' => '08788284476',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 13
        ],[
            'id' => 13,
            'name' => 'veronica',
            'email' => 'veronica@elroichurch.com',
            'password' => Hash::make('password_veronica'),
            'no_hp' => '082177834898',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 14
        ],[
            'id' => 14,
            'name' => 'delima',
            'email' => 'delima@elroichurch.com',
            'password' => Hash::make('password_delima'),
            'no_hp' => '081316206441',
            'role' => 'Cool Leader'
        ]);
        //Chapter 3
        User::updateOrCreate([
            'id' => 15
        ],[
            'id' => 15,
            'name' => 'darsono',
            'email' => 'darsono@elroichurch.com',
            'password' => Hash::make('password_darsono'),
            'no_hp' => '081310707284 ',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 15
        ],[
            'id' => 15,
            'name' => 'darsono',
            'email' => 'darsono@elroichurch.com',
            'password' => Hash::make('password_darsono'),
            'no_hp' => '081310707284',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 16
        ],[
            'id' => 16,
            'name' => 'teddy',
            'email' => 'teddy@elroichurch.com',
            'password' => Hash::make('password_teddy'),
            'no_hp' => '081295091911',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 17
        ],[
            'id' => 17,
            'name' => 'sugianto',
            'email' => 'sugianto@elroichurch.com',
            'password' => Hash::make('password_sugianto'),
            'no_hp' => '08125739397',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 18
        ],[
            'id' => 18,
            'name' => 'feby',
            'email' => 'feby@elroichurch.com',
            'password' => Hash::make('password_feby'),
            'no_hp' => '081263689997',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 19
        ],[
            'id' => 19,
            'name' => 'novi',
            'email' => 'novi@elroichurch.com',
            'password' => Hash::make('password_novi'),
            'no_hp' => '081263689997',
            'role' => 'Cool Leader'
        ]); 
        //Chapter 4
        User::updateOrCreate([
            'id' => 20
        ],[
            'id' => 20,
            'name' => 'hendi',
            'email' => 'hendi@elroichurch.com',
            'password' => Hash::make('password_hendi'),
            'no_hp' => '08115415687',
            'role' => 'Cool Leader'
        ]); 
        User::updateOrCreate([
            'id' => 21
        ],[
            'id' => 21,
            'name' => 'altje',
            'email' => 'altje@elroichurch.com',
            'password' => Hash::make('password_altje'),
            'no_hp' => '087870426929',
            'role' => 'Cool Leader'
        ]); 

        User::updateOrCreate([
            'id' => 22
        ],[
            'id' => 22,
            'name' => 'wempi',
            'email' => 'wempi@elroichurch.com',
            'password' => Hash::make('password_wempi'),
            'no_hp' => '087885050753',
            'role' => 'Cool Leader'
        ]); 
        User::updateOrCreate([
            'id' => 23
        ],[
            'id' => 23,
            'name' => 'charles',
            'email' => 'charles@elroichurch.com',
            'password' => Hash::make('password_charles'),
            'no_hp' => '08176611220',
            'role' => 'Cool Leader'
        ]); 
        User::updateOrCreate([
            'id' => 24
        ],[
            'id' => 24,
            'name' => 'aris',
            'email' => 'aris@elroichurch.com',
            'password' => Hash::make('password_aris'),
            'no_hp' => '08176611220',
            'role' => 'Cool Leader'
        ]); 
        //chapter 5
        User::updateOrCreate([
            'id' => 25
        ],[
            'id' => 25,
            'name' => 'reynaldo',
            'email' => 'reynaldo@elroichurch.com',
            'password' => Hash::make('password_reynaldo'),
            'no_hp' => '081380399398',
            'role' => 'Cool Leader'
        ]); 
        User::updateOrCreate([
            'id' => 26
        ],[
            'id' => 26,
            'name' => 'meliala.s',
            'email' => 'meliala.s@elroichurch.com',
            'password' => Hash::make('password_meliala.s'),
            'no_hp' => '081218844743',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 27
        ],[
            'id' => 27,
            'name' => 'olivia',
            'email' => 'olivia@elroichurch.com',
            'password' => Hash::make('password_olivia'),
            'no_hp' => '082111203469',
            'role' => 'Cool Leader'
        ]); 
        User::updateOrCreate([
            'id' => 28
        ],[
            'id' => 28,
            'name' => 'Evi Somali',
            'email' => 'evi_somali@elroichurch.com',
            'password' => Hash::make('password_evi_somali'),
            'no_hp' => '081293599575',
            'role' => 'Cool Leader'
        ]);
        //Chapter 6
        User::updateOrCreate([
            'id' => 29
        ],[
            'id' => 29,
            'name' => 'Lieke A',
            'email' => 'lieke_a@elroichurch.com',
            'password' => Hash::make('password_lieke_a'),
            'no_hp' => '0817731415',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 30
        ],[
            'id' => 30,
            'name' => 'Esther',
            'email' => 'esther@elroichurch.com',
            'password' => Hash::make('password_esther'),
            'no_hp' => '08121200398',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 31
        ],[
            'id' => 31,
            'name' => 'trihastuti',
            'email' => 'trihastuti@elroichurch.com',
            'password' => Hash::make('password_trihastuti'),
            'no_hp' => '082112228292',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 32
        ],[
            'id' => 32,
            'name' => 'lamsihar',
            'email' => 'lamsihar@elroichurch.com',
            'password' => Hash::make('password_lamsihar'),
            'no_hp' => '081381780141',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 33
        ],[
            'id' => 33,
            'name' => 'Steady Uhing',
            'email' => 'steady_uhing@elroichurch.com',
            'password' => Hash::make('password_steady_uhing'),
            'no_hp' => '081283832348',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 34
        ],[
            'id' => 34,
            'name' => 'hana',
            'email' => 'hana@elroichurch.com',
            'password' => Hash::make('password_hana'),
            'no_hp' => '085691253780',
            'role' => 'Cool Leader'
        ]); 
        //Chapter 7
        User::updateOrCreate([
            'id' => 35
        ],[
            'id' => 35,
            'name' => 'lidya',
            'email' => 'lidya@elroichurch.com',
            'password' => Hash::make('password_lidya'),
            'no_hp' => '081287510829',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 36
        ],[
            'id' => 36,
            'name' => 'lieke_b',
            'email' => 'lieke_b@elroichurch.com',
            'password' => Hash::make('password_lieke_b'),
            'no_hp' => '081287510829',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 37
        ],[
            'id' => 37,
            'name' => 'upie',
            'email' => 'upie@elroichurch.com',
            'password' => Hash::make('password_upie'),
            'no_hp' => '081287510829',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 38
        ],[
            'id' => 38,
            'name' => 'tuti',
            'email' => 'tuti@elroichurch.com',
            'password' => Hash::make('password_tuti'),
            'no_hp' => '081287510829',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 39
        ],[
            'id' => 39,
            'name' => 'tien',
            'email' => 'tien@elroichurch.com',
            'password' => Hash::make('password_tien'),
            'no_hp' => '081287510829',
            'role' => 'Cool Leader'
        ]);
        //Chapter 8A     
        User::updateOrCreate([
            'id' => 40
        ],[
            'id' => 40,
            'name' => 'elsa',
            'email' => 'elsa@elroichurch.com',
            'password' => Hash::make('password_elsa'),
            'no_hp' => '081318278777',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 41
        ],[
            'id' => 41,
            'name' => 'liliana',
            'email' => 'liliana@elroichurch.com',
            'password' => Hash::make('password_liliana'),
            'no_hp' => '081318278777',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 42
        ],[
            'id' => 42,
            'name' => 'rose',
            'email' => 'rose@elroichurch.com',
            'password' => Hash::make('password_rose'),
            'no_hp' => '081318278777',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 43
        ],[
            'id' => 43,
            'name' => 'linda',
            'email' => 'linda@elroichurch.com',
            'password' => Hash::make('password_linda'),
            'no_hp' => '081219076885',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 44
        ],[
            'id' => 44,
            'name' => 'evi_sofia',
            'email' => 'evi_sofia@elroichurch.com',
            'password' => Hash::make('password_evi_sofia'),
            'no_hp' => '081219076885',
            'role' => 'Cool Leader'
        ]);
        //Chapter 8B
        User::updateOrCreate([
            'id' => 45
        ],[
            'id' => 45,
            'name' => 'liana',
            'email' => 'liana@elroichurch.com',
            'password' => Hash::make('password_liana'),
            'no_hp' => '089507055909',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 46
        ],[
            'id' => 46,
            'name' => 'suharyati',
            'email' => 'suharyati@elroichurch.com',
            'password' => Hash::make('password_suharyati'),
            'no_hp' => '081293731165',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 47
        ],[
            'id' => 47,
            'name' => 'rina',
            'email' => 'rina@elroichurch.com',
            'password' => Hash::make('password_rina'),
            'no_hp' => '081311324050',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 48
        ],[
            'id' => 48,
            'name' => 'giok_hwa',
            'email' => 'giok_hwa@elroichurch.com',
            'password' => Hash::make('password_giok_hwa'),
            'no_hp' => '081311324050',
            'role' => 'Cool Leader'
        ]);
        //Chapter 9
        User::updateOrCreate([
            'id' => 49
        ],[
            'id' => 49,
            'name' => 'david',
            'email' => 'david@elroichurch.com',
            'password' => Hash::make('password_david'),
            'no_hp' => '081316184523',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 50
        ],[
            'id' => 50,
            'name' => 'rudi',
            'email' => 'rudi@elroichurch.com',
            'password' => Hash::make('password_rudi'),
            'no_hp' => '081395954992',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 51
        ],[
            'id' => 51,
            'name' => 'jonius',
            'email' => 'jonius@elroichurch.com',
            'password' => Hash::make('password_jonius'),
            'no_hp' => '085891325469',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 52
        ],[
            'id' => 52,
            'name' => 'serry',
            'email' => 'serry@elroichurch.com',
            'password' => Hash::make('password_serry'),
            'no_hp' => '08111112870',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 53
        ],[
            'id' => 53,
            'name' => 'martin',
            'email' => 'martin@elroichurch.com',
            'password' => Hash::make('password_martin'),
            'no_hp' => '081218186720',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 54
        ],[
            'id' => 54,
            'name' => 'idris',
            'email' => 'idris@elroichurch.com',
            'password' => Hash::make('password_idris'),
            'no_hp' => '081281291678',
            'role' => 'Cool Leader'
        ]);


        User::updateOrCreate([
            'id' => 55
        ],[
            'id' => 55,
            'name' => 'edwin',
            'email' => 'edwin@elroichurch.com',
            'password' => Hash::make('password_edwin'),
            'no_hp' => '081383330212',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 56
        ],[
            'id' => 56,
            'name' => 'dadang',
            'email' => 'dadang@elroichurch.com',
            'password' => Hash::make('password_dadang'),
            'no_hp' => '08111713202',
            'role' => 'Cool Leader'
        ]);
        

        User::updateOrCreate([
            'id' => 57
        ],[
            'id' => 57,
            'name' => 'eka',
            'email' => 'eka@elroichurch.com',
            'password' => Hash::make('password_eka'),
            'no_hp' => '082211626226',
            'role' => 'Cool Leader'
        ]);
        //Chapter 10
        User::updateOrCreate([
            'id' => 58
        ],[
            'id' => 58,
            'name' => 'elang',
            'email' => 'elang@elroichurch.com',
            'password' => Hash::make('password_elang'),
            'no_hp' => '081280234321',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 59
        ],[
            'id' => 59,
            'name' => 'daniel',
            'email' => 'daniel@elroichurch.com',
            'password' => Hash::make('password_daniel'),
            'no_hp' => '081280234321',
            'role' => 'Cool Leader'
        ]);
        

        User::updateOrCreate([
            'id' => 60
        ],[
            'id' => 60,
            'name' => 'nia',
            'email' => 'nia@elroichurch.com',
            'password' => Hash::make('password_nia'),
            'no_hp' => '081366660768',
            'role' => 'Cool Leader'
        ]);
        
        User::updateOrCreate([
            'id' => 61
        ],[
            'id' => 61,
            'name' => 'gretty',
            'email' => 'gretty@elroichurch.com',
            'password' => Hash::make('password_gretty'),
            'no_hp' => '087885432671',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 62
        ],[
            'id' => 62,
            'name' => 'agnes',
            'email' => 'agnes@elroichurch.com',
            'password' => Hash::make('password_agnes'),
            'no_hp' => '081366660768',
            'role' => 'Cool Leader'
        ]);

        

       

        User::updateOrCreate([
            'id' => 63
        ],[
            'id' => 63,
            'name' => 'grace',
            'email' => 'grace@elroichurch.com',
            'password' => Hash::make('password_grace'),
            'no_hp' => '087884514468',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 64
        ],[
            'id' => 64,
            'name' => 'dinna',
            'email' => 'dinna@elroichurch.com',
            'password' => Hash::make('password_dinna'),
            'no_hp' => '089510217890',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 65
        ],[
            'id' => 65,
            'name' => 'Daniel C',
            'email' => 'daniel_c@elroichurch.com',
            'password' => Hash::make('password_daniel_c'),
            'no_hp' => '08991925299',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 66
        ],[
            'id' => 66,
            'name' => 'Agatha',
            'email' => 'agatha@elroichurch.com',
            'password' => Hash::make('password_agatha'),
            'no_hp' => '089624283824',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 67
        ],[
            'id' => 67,
            'name' => 'jordan',
            'email' => 'jordan@elroichurch.com',
            'password' => Hash::make('password_jordan'),
            'no_hp' => '087780250044',
            'role' => 'Cool Leader'
        ]);

        User::updateOrCreate([
            'id' => 68
        ],[
            'id' => 68,
            'name' => 'jezzlin',
            'email' => 'jezzlin@elroichurch.com',
            'password' => Hash::make('password_jezzlin'),
            'no_hp' => '085959704319',
            'role' => 'Cool Leader'
        ]);
        User::updateOrCreate([
            'id' => 69
        ],[
            'id' => 69,
            'name' => 'martha',
            'email' => 'martha@elroichurch.com',
            'password' => Hash::make('password_martha'),
            'no_hp' => '0895381863427',
            'role' => 'Cool Leader'
        ]);
    }
}
