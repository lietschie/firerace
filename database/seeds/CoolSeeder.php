<?php

use Illuminate\Database\Seeder;
use App\Cool;
class CoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cool::updateOrCreate([
            'id' => 1
        ],[
            'id' => 1,
            'name' => 'No Cool',
        ]);
        
        Cool::updateOrCreate([
            'id' => 2,
        ],[
            'id' => 2,
            'name' => '1.001',
            'location' => 'Rivaria'
        ]);

        Cool::updateOrCreate([
            'id' => 3,
        ],[
            'id' => 3,
            'name' => '1.002',
            'location' => 'Rivaria'
        ]);

        Cool::updateOrCreate([
            'id' => 4,
        ],[
            'id' => 4,
            'name' => '1.003',
            'location' => 'Bedahan'
        ]);
        Cool::updateOrCreate([
            'id' => 5,
        ],[
            'id' => 5,
            'name' => '1.004',
            'location' => 'Bedahan'
        ]);
        Cool::updateOrCreate([
            'id' => 5,
        ],[
            'id' => 5,
            'name' => '1.005',
            'location' => 'ERC Sawangan'
        ]);

        Cool::updateOrCreate([
            'id' => 6,
        ],[
            'id' => 6,
            'name' => '2.001',
            'location' => 'Bedahan',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 7,
        ],[
            'id' => 7,
            'name' => '2.002',
            'location' => 'Bojongsari',
            'default_number_of_meeting' => 2

        ]);

        Cool::updateOrCreate([
            'id' => 8,
        ],[
            'id' => 8,
            'name' => '2.003',
            'location' => 'Aliandong',
            'default_number_of_meeting' => 2

        ]);
       
        Cool::updateOrCreate([
            'id' => 9,
        ],[
            'id' => 9,
            'name' => '2.004',
            'location' => 'Aliandong ',
            'default_number_of_meeting' => 4

        ]);

        Cool::updateOrCreate([
            'id' => 10,
        ],[
            'id' => 10,
            'name' => '2.005',
            'location' => 'Aliandong',
            'default_number_of_meeting' => 3
        ]);
        //Chapter 3
        Cool::updateOrCreate([
            'id' => 11,
        ],[
            'id' => 11,
            'name' => '3.001',
            'location' => 'DTC',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 12,
        ],[
            'id' => 12,
            'name' => '3.002',
            'location' => 'Beiji Depok',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 13,
        ],[
            'id' => 13,
            'name' => '3.003',
            'location' => 'Telaga Golf',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 14,
        ],[
            'id' => 14,
            'name' => '3.004',
            'location' => 'Curug Topik',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 15,
        ],[
            'id' => 15,
            'name' => '3.005',
            'location' => 'Pondok Petir',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 4
        Cool::updateOrCreate([
            'id' => 16,
        ],[
            'id' => 16,
            'name' => '4.001',
            'location' => 'Bedahan',
            'default_number_of_meeting' => 2
        ]);
        
        Cool::updateOrCreate([
            'id' => 17,
        ],[
            'id' => 17,
            'name' => '4.002',
            'location' => 'Kampung Bulu',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 18,
        ],[
            'id' => 18,
            'name' => '4.003',
            'location' => 'BSI ',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 19,
        ],[
            'id' => 19,
            'name' => '4.004',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 20,
        ],[
            'id' => 20,
            'name' => '4.005',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 5
        Cool::updateOrCreate([
            'id' => 21,
        ],[
            'id' => 21,
            'name' => '5.001',
            'location' => 'Bojong Sari',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 22,
        ],[
            'id' => 22,
            'name' => '5.002',
            'location' => 'Bojong Sari',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 23,
        ],[
            'id' => 23,
            'name' => '5.003',
            'location' => 'Waru Induk',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 24,
        ],[
            'id' => 24,
            'name' => '5.004',
            'location' => 'Parung',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 25,
        ],[
            'id' => 25,
            'name' => '5.005',
            'location' => 'Cendekia',
            'default_number_of_meeting' => 3
        ]);
        Cool::updateOrCreate([
            'id' => 26,
        ],[
            'id' => 26,
            'name' => '5.006',
            'location' => 'Telaga Kahuripan',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 6
        Cool::updateOrCreate([
            'id' => 27,
        ],[
            'id' => 27,
            'name' => '6.001',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 28,
        ],[
            'id' => 28,
            'name' => '6.002',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 29,
        ],[
            'id' => 29,
            'name' => '6.003',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 30,
        ],[
            'id' => 30,
            'name' => '6.004',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 31,
        ],[
            'id' => 31,
            'name' => '6.005',
            'location' => 'Meruyung',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 32,
        ],[
            'id' => 32,
            'name' => '6.006',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 7
        Cool::updateOrCreate([
            'id' => 33,
        ],[
            'id' => 33,
            'name' => '7.001',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 34,
        ],[
            'id' => 34,
            'name' => '7.002',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 35,
        ],[
            'id' => 35,
            'name' => '7.003',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 36,
        ],[
            'id' => 36,
            'name' => '7.004',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 37,
        ],[
            'id' => 37,
            'name' => '7.005',
            'location' => 'WKA',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 8A
        Cool::updateOrCreate([
            'id' => 38,
        ],[
            'id' => 38,
            'name' => '8A.001',
            'location' => 'Vila Dago',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 38,
        ],[
            'id' => 38,
            'name' => '8A.002',
            'location' => 'Parung',
            'default_number_of_meeting' => 4
        ]);
        Cool::updateOrCreate([
            'id' => 39,
        ],[
            'id' => 39,
            'name' => '8A.003',
            'location' => 'Parung',
            'default_number_of_meeting' => 4
        ]);
        Cool::updateOrCreate([
            'id' => 40,
        ],[
            'id' => 40,
            'name' => '8A.004',
            'location' => 'Puri Bali',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 41,
        ],[
            'id' => 41,
            'name' => '8A.005',
            'location' => 'DTC',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 8B
        Cool::updateOrCreate([
            'id' => 42,
        ],[
            'id' => 42,
            'name' => '8B.001',
            'location' => 'Bojong Sari',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 43,
        ],[
            'id' => 43,
            'name' => '8B.002',
            'location' => 'BSI',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 44,
        ],[
            'id' => 44,
            'name' => '8B.003',
            'location' => 'Telaga Golf',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 45,
        ],[
            'id' => 45,
            'name' => '8B.004',
            'location' => 'Bojongsari',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 9
        Cool::updateOrCreate([
            'id' => 46,
        ],[
            'id' => 46,
            'name' => '9.001',
            'location' => 'Telaga Golf',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 47,
        ],[
            'id' => 47,
            'name' => '9.002',
            'location' => 'ERC / SICC / TDRP',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 48,
        ],[
            'id' => 48,
            'name' => '9.003',
            'location' => 'Brandweer',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 49,
        ],[
            'id' => 49,
            'name' => '9.004',
            'location' => 'Parung',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 50,
        ],[
            'id' => 50,
            'name' => '9.005',
            'location' => 'Kahuripan',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 51,
        ],[
            'id' => 51,
            'name' => '9.006',
            'location' => 'Kahuripan',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 52,
        ],[
            'id' => 52,
            'name' => '9.007',
            'location' => 'Bojong Sari',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 53,
        ],[
            'id' => 53,
            'name' => '9.008',
            'location' => 'Bojong Sari',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 54,
        ],[
            'id' => 54,
            'name' => '9.009',
            'location' => 'Sawangan',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 55,
        ],[
            'id' => 55,
            'name' => '9.010',
            'location' => 'Kalisuren',
            'default_number_of_meeting' => 2
        ]);
       
        Cool::updateOrCreate([
            'id' => 56,
        ],[
            'id' => 56,
            'name' => '9.011',
            'location' => 'Rawa Kalong/ Gn.Sindur ',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 57,
        ],[
            'id' => 57,
            'name' => '9.012',
            'location' => 'Pondok Petir',
            'default_number_of_meeting' => 2
        ]);
        //Chapter 10
        Cool::updateOrCreate([
            'id' => 58,
        ],[
            'id' => 58,
            'name' => '10.001',
            'location' => 'Telaga Golf 1',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 59,
        ],[
            'id' => 59,
            'name' => '10.002',
            'location' => 'Telaga Golf 2',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 60,
        ],[
            'id' => 60,
            'name' => '10.003',
            'location' => 'Pamulang - BSD',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 61,
        ],[
            'id' => 61,
            'name' => '10.004',
            'location' => 'Rivaria',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 62,
        ],[
            'id' => 62,
            'name' => '10.005',
            'location' => 'Parung',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 64,
        ],[
            'id' => 64,
            'name' => '10.006',
            'location' => 'BSI',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 65,
        ],[
            'id' => 65,
            'name' => '10.007',
            'location' => 'Pengasinan',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 66,
        ],[
            'id' => 66,
            'name' => '10.008',
            'location' => 'Bedahan',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 67,
        ],[
            'id' => 67,
            'name' => '10.009',
            'location' => 'Kahuripan',
            'default_number_of_meeting' => 2
        ]);
        
        Cool::updateOrCreate([
            'id' => 69,
        ],[
            'id' => 69,
            'name' => '10.010',
            'location' => 'Bojong Sari 1',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 70,
        ],[
            'id' => 70,
            'name' => '10.011',
            'location' => 'Pancoran Mas',
            'default_number_of_meeting' => 2
        ]);
        Cool::updateOrCreate([
            'id' => 71,
        ],[
            'id' => 71,
            'name' => '10.012',
            'location' => 'Sawangan Elok',
            'default_number_of_meeting' => 2
        ]);

        Cool::updateOrCreate([
            'id' => 72,
        ],[
            'id' => 72,
            'name' => '10.013',
            'location' => 'Bojong Sari 2',
            'default_number_of_meeting' => 2
        ]);
       
        
    }
}
