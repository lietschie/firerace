<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createUser = User::updateOrCreate([
            'id' => 1
        ],[
            'id' => 1,
            'name' => 'Admin',
            'email' => 'admin@elroichurch.com',
            'password' => Hash::make('secret'),
            'role' => 'super-admin'
        ]);

        $createUser->pivotCool()->attach(1);

        $createUser = User::updateOrCreate([
            'id' => 2
        ],[
            'id' => 2,
            'name' => 'Kezia',
            'email' => 'kezia@elroichurch.com',
            'password' => Hash::make('password_kezia'),
            'role' => 'admin'
        ]);

        $createUser->pivotCool()->attach(1);


        $createUser = User::updateOrCreate([
            'id' => 3
        ],[
            'id' => 3,
            'name' => 'Yongky',
            'email' => 'yongky@elroichurch.com',
            'password' => Hash::make('password_yongky'),
            'role' => 'admin'
        ]);

        $createUser->pivotCool()->attach(1);


        $createUser = User::updateOrCreate([
            'id' => 4
        ],[
            'id' => 4,
            'name' => 'Hendy',
            'email' => 'hendy@elroichurch.com',
            'password' => Hash::make('password_hendy'),
            'role' => 'admin'
        ]);

        $createUser->pivotCool()->attach(1);

    }
}
