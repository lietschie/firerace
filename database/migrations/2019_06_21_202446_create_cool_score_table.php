<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoolScoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cool_score', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('periode_id');
            $table->unsignedBigInteger('cool_id');
            $table->unsignedBigInteger('church_id');
            $table->unsignedBigInteger('parameter_id');
            $table->integer('score')->default(0);
            $table->integer('qty')->default(0);
            $table->integer('total_score')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cool_score');
    }
}
