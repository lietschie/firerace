<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnggotaCoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota_cool', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cool_id');
            $table->unsignedBigInteger('user_id');
            $table->integer('type')->default(1);

            $table->foreign('user_id')->on('users')->references('id')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('cool_id')->on('cool')->references('id')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota_cool');
    }
}
