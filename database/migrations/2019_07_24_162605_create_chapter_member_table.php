<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChapterMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapter_member', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cool_id');
            $table->unsignedBigInteger('chapter_id');

            $table->foreign('cool_id')->on('cool')->references('id')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('chapter_id')->on('chapter')->references('id')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapter_member');
    }
}
