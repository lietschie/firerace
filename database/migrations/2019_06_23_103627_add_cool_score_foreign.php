<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoolScoreForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cool_score', function (Blueprint $table) {
            //
            $table->foreign('periode_id')->on('periode')->references('id')->onDelete('cascade');
            $table->foreign('cool_id')->on('cool')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cool_score', function (Blueprint $table) {
            //
            $table->dropForeign(['periode_id']);
            $table->dropForeign(['cool_id']);
        });
    }
}
