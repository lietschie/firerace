<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cool', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('leader_id')->on('users')
            ->references('id')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->string('location')->nullable();
            $table->integer('default_number_of_meeting')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cool', function (Blueprint $table) {
            //
            // $table->dropForeign(['leader_id']);
            $table->dropColumn('leader_id');
            $table->dropColumn('location');
            $table->dropColumn('default_number_of_meeting');
        });
    }
}
