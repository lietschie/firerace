<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@index')->name('web.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->prefix('admin')->group(function(){

    Route::prefix('cool')->group(function(){
        Route::get('/','CoolController@index')->name('cool.admin.index');
        Route::get('/create','CoolController@create')->name('cool.admin.create');
        Route::post('/store','CoolController@store')->name('cool.admin.store');
        Route::get('/edit/{id}','CoolController@edit')->name('cool.admin.edit');
        Route::get('/detail/{id}','CoolController@detail')->name('cool.admin.detail');
        Route::post('/update/{id}','CoolController@update')->name('cool.admin.update');
        Route::get('/delete/{id}','CoolController@delete')->name('cool.admin.delete');

        Route::get('/detail/{id}/periode/{periode}','CoolController@detailCoolPeriode')->name('cool.admin.detail-periode');
        Route::get('/detail/{id}/periode/{periode}/score/{score}','CoolController@deleteCoolPeriodeScore')->name('cool.admin.detail-periode-scoredelete');
        Route::get('/input-score','CoolController@inputScore')->name('cool.admin.input-score');
        Route::get('/detail/{id}/detach-anggota/{user_id}','CoolController@detachAnggota')->name('cool.admin.detach-anggota');
        Route::post('/detail/{id}/attach-anggota','CoolController@attachAnggota')->name('cool.admin.attach-anggota');
        Route::post('/store-score','CoolController@storeScore')->name('cool.admin.store-score');
        Route::get('/generate-score','CoolController@generateScore')->name('cool.admin.generate-score');

    });
    Route::prefix('user')->group(function(){
        Route::get('/','UserController@index')->name('user.admin.index');
        Route::get('/create','UserController@create')->name('user.admin.create');
        Route::post('/store','UserController@store')->name('user.admin.store');
        Route::get('/edit/{id}','UserController@edit')->name('user.admin.edit');
        Route::post('/update/{id}','UserController@update')->name('user.admin.update');
        Route::get('/delete/{id}','UserController@delete')->name('user.admin.delete');
    });
    Route::prefix('parameter')->group(function(){
        Route::get('/','ParameterController@index')->name('parameter.admin.index');
        Route::get('/create','ParameterController@create')->name('parameter.admin.create');
        Route::post('/store','ParameterController@store')->name('parameter.admin.store');
        Route::get('/edit/{id}','ParameterController@edit')->name('parameter.admin.edit');
        Route::post('/update/{id}','ParameterController@update')->name('parameter.admin.update');
        Route::get('/delete/{id}','ParameterController@delete')->name('parameter.admin.delete');
    });

    Route::prefix('periode')->group(function(){
        Route::get('/','PeriodeController@index')->name('periode.admin.index');
        Route::get('/create','PeriodeController@create')->name('periode.admin.create');
        Route::post('/store','PeriodeController@store')->name('periode.admin.store');
        Route::get('/edit/{id}','PeriodeController@edit')->name('periode.admin.edit');
        Route::post('/update/{id}','PeriodeController@update')->name('periode.admin.update');
        Route::get('/delete/{id}','PeriodeController@delete')->name('periode.admin.delete');
    });
    Route::prefix('chapter')->group(function(){
        Route::get('/','ChapterController@index')->name('chapter.admin.index');
        Route::get('/create','ChapterController@create')->name('chapter.admin.create');
        Route::post('/store','ChapterController@store')->name('chapter.admin.store');
        Route::get('/edit/{id}','ChapterController@edit')->name('chapter.admin.edit');
        Route::get('/detail/{id}','ChapterController@detail')->name('chapter.admin.detail');
        Route::get('/detail/{id}/detach-anggota/{user_id}','ChapterController@detachAnggota')->name('chapter.admin.detach-anggota');
        Route::post('/detail/{id}/attach-anggota','ChapterController@attachAnggota')->name('chapter.admin.attach-anggota');
        // Route::get('/detail/{id}/','ChapterController@detail')->name('chapter.admin.attach-anggota');
        Route::post('/update/{id}','ChapterController@update')->name('chapter.admin.update');
        Route::get('/delete/{id}','ChapterController@delete')->name('chapter.admin.delete');
    });

});
