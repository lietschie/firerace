<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChapterMember extends Model
{
    //
    public $table = 'chapter_member';
}
