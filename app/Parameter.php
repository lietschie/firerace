<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    //

    public $table = 'parameter';

    public $fillable = [
        'name',
        'score',
        'description',
        'type'
    ];  
}
