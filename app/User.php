<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'email','no_hp','password','role','address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pivotCool(){
        return $this->belongsToMany(Cool::class,'anggota_cool','user_id','cool_id');
    }

    public function cool(){
        return $this->belongsTo(Cool::class,'cool_id');
    }

    public function chapter(){
        return $this->hasOne(Chapter::class,'pic_id','id');
    }

    public function getChapterLeader(){
        if($this->role == ''){
            return $this->chapter;
        }   
        return false;
    }
}
