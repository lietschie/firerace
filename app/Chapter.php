<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Chapter extends Model
{
    //

    public $table = 'chapter';

    public $fillable = [
        'name',
        'pic_id',
        'location'
    ];  

    public function pic(){
        return $this->belongsTo(User::class,'pic_id','id');
    }

    public function pivotCool(){
        return $this->belongsToMany(Cool::class,'chapter_member','chapter_id','cool_id');
    }
}
