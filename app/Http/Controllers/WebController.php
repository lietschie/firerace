<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cool;
class WebController extends Controller
{
    //
    private $getCool;
    public function __construct()
    {   
        $this->getCool = new Cool();
    }

    public function index(){
        $data['listCool'] = $this->getCool->orderBy('total_point','desc')->get();
        return view('welcome',$data);
    }
}
