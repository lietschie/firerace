<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cool;
use App\User;
use App\Periode;
use App\Parameter;
use App\CoolScore;
use App\Chapter;
use DB;

class CoolController extends Controller
{
    private $route;
    private $view;
    private $getCool;
    private $getPeriode;
    private $getParameter;
    private $getCoolScore;
    public function __construct()
    {
        $this->route = 'cool.admin';
        $this->view = 'cool';
        $this->model = 'cool';
        $this->getCool = new Cool;
        $this->getUser = new User;
        $this->getChapter = new Chapter;

        $this->getPeriode = new Periode();
        $this->getParameter = new Parameter();
        $this->getCoolScore = new CoolScore();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        // $data[$this->model] = $this->getCool->orderBy('name','asc')->get();
        $getUserAuth = auth()->user();
        $data[$this->model] = $this->getCool->orderBy('name','asc')->get();
        if($getUserAuth->is_chapterleader){
            $data[$this->model] = $this->getChapter->find($getUserAuth->chapter->id)->pivotCool()->orderBy('name','asc')->get();
        }
        return view($this->view . '.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data['leaders'] = $this->getUser->where('role','Cool Leader')->get();
        return view($this->view . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storedData = [
            'name' => $request->name,
        ];

        $createCool = $this->getCool->create($storedData);
        if ($createCool) {
            return redirect()->route($this->route . '.index')->with('success_message', 'Create Cool Berhasil');
        }
        return redirect()->route($this->route . '.index')->with('error_message', 'Create Cool Gagal');
    }

    public function inputScore()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $getUserAuth = auth()->user();
        $data['listCool'] = $this->getCool->get();
        if($getUserAuth->is_chapterleader){
            $data['listCool'] = $this->getChapter->find($getUserAuth->chapter->id)->pivotCool;
        }
        $data['listParameter'] = $this->getParameter->get();
        $data['listPeriode'] = $this->getPeriode->get();
        return view($this->view . '.input_score', $data);
    }

    public function generateScore()
    {

        $getAllCool = $this->getCool->get();
        foreach ($getAllCool as $cool) {
            $totalScore = $this->sumCoolScore($cool->id);
            $update = $cool->update([
                'total_point' => $totalScore
            ]);
        }
        if ($update) {
            return redirect()->back()->with('success_message', 'Berhasil Generate');
        }
        return redirect()->back()->with('error_message', 'Gagal Generate');
    }

    public function sumCoolScore($id)
    {
        // $find = $this->getCoolScore->where('id',$id)->first();
        // return $find ? $find->sum('total_score') : 0;
        return $this->getCoolScore->where('cool_id', $id)->sum('total_score');
    }

    public function storeScore(Request $request)
    {
        // dd($request->all());
        $periode = $request->periode_id;
        $cool = $request->cool_id;

        $getCoolScore = $this->getCoolScore
        ->where('periode_id', $periode)
        ->where('cool_id', $cool)->count() > 0 
        && auth()->user()->role != 'super-admin' && auth()->user()->role != 'admin' && auth()->user()->role != 'Cool Leader';
        if (!$getCoolScore) {
            $point = 0;
            foreach ($request->parameter as $key =>  $param) {
                // if($param['type'] == 'check'){
                $paramID = $key;
                $checkQty = array_key_exists('qty', $param);
                $qty =  $checkQty ?  $param['qty'] : 0;
                $score = $param['score'];
                $type = $param['type'];
                $total_score = $qty * $score;

                $point += $total_score;

                $data = [
                    'parameter_id' => $paramID,
                    'periode_id' =>  $periode,
                    'cool_id' => $cool,
                    'qty' =>  $qty,
                    'score' =>  $score,
                    'type' =>  $type,
                    'total_score' => $total_score
                ];

                $this->getCoolScore->updateOrCreate([
                    'parameter_id' => $paramID,
                    'periode_id' =>  $periode,
                    'cool_id' => $cool
                ], $data);
            }
            $this->generateScore();
            return redirect()->route($this->route . '.index')->with('success_message', 'Berhasil Input Nilai');
        }
        return redirect()->route($this->route . '.index')->with('error_message', 'Gagal Input Nilai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {

        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getCool->with('hasManyScore')->find($id);
        return view($this->view . '.detail', $data);
    }

    public function detailCoolPeriode($id,$periode){
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data['periode'] = $this->getPeriode->find($periode);
        $data[$this->model] = $this->getCool->with(['hasManyScore' => function($query) use ($periode){
            $query->where('periode_id',$periode);
        },'hasManyScore.parameter'])->find($id);
        return view($this->view . '.periode-cool', $data);
    }

    public function deleteCoolPeriodeScore($id,$periode,$scoreId){
        $getCool = $this->getCool->find($id)->hasManyScore()->where('periode_id',$periode)->where('id',$scoreId)->delete();
        if($getCool){
            return redirect()->route($this->route.'.index')->with('success_message','Berhasil Delete Data');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Gagal Delete Data');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getCool->find($id);
        $data['leaders'] = $this->getUser->where('role','Cool Leader')->get();
        $data['listUser'] = $this->getUser->where('role','user')->get();        
        return view($this->view . '.edit', $data);
    }

    public function attachAnggota($cool_id, Request $request)
    {
        $this->getCool->find($cool_id)->pivotUser()->attach($request->user_id);
        $this->getUser->find($request->user_id)->update([
            'cool_id' => $cool_id
        ]);
        return redirect()->back()->with('success_message', 'Succcess Attach Anggota');
    }

    public function detachAnggota($cool_id, $user_id)
    {
        $this->getCool->find($cool_id)->pivotUser()->detach($user_id);
        return redirect()->back()->with('success_message', 'Succcess Delete Anggota');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $storedData = [
            'name' => $request->name,
            'leader_id' => $request->leader_id,
            'location' => $request->location
        ];

        $updateCool = $this->getCool->findOrFail($id)->update($storedData);
        if ($updateCool) {
            return redirect()->route($this->route . '.index')->with('success_message', 'Update Cool Berhasil');
        }
        return redirect()->route($this->route . '.index')->with('error_message', 'Update Cool Gagal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $updateCool = $this->getCool->findOrFail($id)->delete();
        if ($updateCool) {
            return redirect()->route($this->route . '.index')->with('success_message', 'Delete Cool Berhasil');
        }
        return redirect()->route($this->route . '.index')->with('error_message', 'Delete Cool Gagal');
    }
}
