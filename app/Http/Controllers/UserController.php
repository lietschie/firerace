<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cool;
class UserController extends Controller
{
    private $route;
    private $view;
    private $getUser;
    public function __construct()
    {   
        $this->route = 'user.admin';
        $this->view = 'user';
        $this->model = 'user';
        $this->getUser = new User;
        $this->getCool = new Cool;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $getAuth = User::find(auth()->id());
        if($getAuth->role == "super-admin"){
            $data[$this->model] = $this->getUser->with('cool')->get();
        }else{
            $data[$this->model] = $this->getUser->with('cool')->where('role','user')->get();
        }
        return view($this->view.'.index',$data);
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data['coolList'] = $this->getCool->get();
        return view($this->view.'.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storedData = [
            'name' => $request->name,
            'no_hp' => $request->no_hp,
            'email' => $request->email?? null,
            'address' => $request->address
        ];
        $storedData['password'] = $request->password ? bcrypt($request->password) : bcrypt('secret12345') ;

        $createUser = $this->getUser->create($storedData);
        if($createUser) {
            // $createUser->pivotCool()->attach($request->cool_id); // attaching to pivot table anggota_cool
            return redirect()->route($this->route.'.index')->with('success_message','Create User Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Create User Gagal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getUser->find($id);
        $data['coolList'] = $this->getCool->get();

        return view($this->view.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $storedData = [
            'name' => $request->name,
            'no_hp' => $request->no_hp,
            'email' => $request->email?? null,
            'address' => $request->address

        ];
        if($request->password){
            $storedData['password'] = bcrypt($request->password);
        }
        $getUser = $this->getUser->find($id);
        $updateUser = $getUser->update($storedData);
        if($updateUser) {
            // $getUser->pivotCool()->detach($getUser->cool_id); // detaching existing to pivot table anggota_cool
            // $getUser->pivotCool()->attach($request->cool_id); // attaching to pivot table anggota_cool
            return redirect()->route($this->route.'.index')->with('success_message','Update User Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Update User Gagal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $updateUser = $this->getUser->findOrFail($id)->delete();
        if($updateUser) {
            return redirect()->route($this->route.'.index')->with('success_message','Delete User Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Delete User Gagal');
    }
}
