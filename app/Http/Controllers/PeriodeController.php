<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periode;
class PeriodeController extends Controller
{
    private $route;
    private $view;
    private $getPeriode;
    public function __construct()
    {   
        $this->route = 'periode.admin';
        $this->view = 'periode';
        $this->model = 'periode';
        $this->getPeriode = new Periode;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getPeriode->orderBy('tanggal','asc')->get();
        return view($this->view.'.index',$data);
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        return view($this->view.'.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storedData = [
            'name' => $request->name,
            'tanggal' => $request->tanggal
        ];

        $createPeriode = $this->getPeriode->create($storedData);
        if($createPeriode) {
            return redirect()->route($this->route.'.index')->with('success_message','Create Periode Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Create Periode Gagal');
    }

    public function inputScore(){
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data['listPeriode'] = $this->getPeriode->get();
        return view($this->view.'.input_score',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getPeriode->find($id);
        
        return view($this->view.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $storedData = [
            'name' => $request->name,
            'tanggal' => $request->tanggal
        ];

        $updatePeriode = $this->getPeriode->findOrFail($id)->update($storedData);
        if($updatePeriode) {
            return redirect()->route($this->route.'.index')->with('success_message','Update Periode Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Update Periode Gagal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $updatePeriode = $this->getPeriode->findOrFail($id)->delete();
        if($updatePeriode) {
            return redirect()->route($this->route.'.index')->with('success_message','Delete Periode Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Delete Periode Gagal');
    }
}
