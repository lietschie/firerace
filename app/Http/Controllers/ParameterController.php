<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parameter;
class ParameterController extends Controller
{
    private $route;
    private $view;
    private $getParameter;
    public function __construct()
    {   
        $this->route = 'parameter.admin';
        $this->view = 'parameter';
        $this->model = 'parameter';
        $this->getParameter = new Parameter;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getParameter->get();
        return view($this->view.'.index',$data);
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        return view($this->view.'.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storedData = [
            'name' => $request->name,
            'score' => $request->score,
            'description' => $request->description,
            'type' => $request->type
        ];

        $createParameter = $this->getParameter->create($storedData);
        if($createParameter) {
            return redirect()->route($this->route.'.index')->with('success_message','Create Parameter Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Create Parameter Gagal');
    }

    public function inputScore(){
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data['listParameter'] = $this->getParameter->get();
        return view($this->view.'.input_score',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getParameter->find($id);
        
        return view($this->view.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $storedData = [
            'name' => $request->name,
            'score' => $request->score,
            'description' => $request->description,
            'type' => $request->type
        ];

        $updateParameter = $this->getParameter->findOrFail($id)->update($storedData);
        if($updateParameter) {
            return redirect()->route($this->route.'.index')->with('success_message','Update Parameter Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Update Parameter Gagal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $updateParameter = $this->getParameter->findOrFail($id)->delete();
        if($updateParameter) {
            return redirect()->route($this->route.'.index')->with('success_message','Delete Parameter Berhasil');
        }
        return redirect()->route($this->route.'.index')->with('error_message','Delete Parameter Gagal');
    }
}
