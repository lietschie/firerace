<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chapter;
use App\User;
use App\Cool;
use App\ChapterMember;
use DB;

class ChapterController extends Controller
{
    private $route;
    private $view;
    private $getChapter;
    private $getPeriode;
    private $getParameter;
    private $getChapterMember;
    public function __construct()
    {
        $this->route = 'chapter.admin';
        $this->view = 'chapter';
        $this->model = 'Chapter';
        $this->getChapter = new Chapter;
        $this->getUser = new User;
        $this->getCool = new Cool;
        $this->getChapterMember = new ChapterMember();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getChapter->get();
        return view($this->view . '.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data['listUsers'] = $this->getUser->get(); 
        return view($this->view . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storedData = [
            'pic_id' => $request->pic_id,
            'name' => $request->name,
        ];

        $createChapter = $this->getChapter->create($storedData);
        if ($createChapter) {
            return redirect()->route($this->route . '.index')->with('success_message', 'Create Chapter Berhasil');
        }
        return redirect()->route($this->route . '.index')->with('error_message', 'Create Chapter Gagal');
    }

    public function inputScore()
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data['listChapter'] = $this->getChapter->get();
        $data['listParameter'] = $this->getParameter->get();
        $data['listPeriode'] = $this->getPeriode->get();
        return view($this->view . '.input_score', $data);
    }

    public function generateScore()
    {

        $getAllChapter = $this->getChapter->get();
        foreach ($getAllChapter as $Chapter) {
            $totalScore = $this->sumChapterScore($Chapter->id);
            $update = $Chapter->update([
                'total_point' => $totalScore
            ]);
        }
        if ($update) {
            return redirect()->back()->with('success_message', 'Berhasil Generate');
        }
        return redirect()->back()->with('error_message', 'Gagal Generate');
    }

    public function sumChapterScore($id)
    {
        // $find = $this->getChapterScore->where('id',$id)->first();
        // return $find ? $find->sum('total_score') : 0;
        return $this->getChapterScore->where('chapter_id', $id)->sum('total_score');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {

        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getChapter->with('hasManyScore')->find($id);
        return view($this->view . '.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['route'] = $this->route;
        $data['model'] = $this->model;
        $data[$this->model] = $this->getChapter->find($id);
        $data['listUsers'] = $this->getUser->get(); 
        $data['listCool'] = $this->getCool->get();
        
        return view($this->view . '.edit', $data);
    }

    public function attachAnggota($chapter_id, Request $request)
    {
        $this->getChapter->find($chapter_id)->pivotCool()->attach($request->cool_id);
        
        return redirect()->back()->with('success_message', 'Succcess Attach Anggota');
    }

    public function detachAnggota($chapter_id, $cool_id)
    {
        $this->getChapter->find($chapter_id)->pivotCool()->detach($cool_id);
        return redirect()->back()->with('success_message', 'Succcess Delete Anggota');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $storedData = [
            'name' => $request->name,
            'pic_id' => $request->pic_id,
        ];

        $updateChapter = $this->getChapter->findOrFail($id)->update($storedData);
        if ($updateChapter) {
            return redirect()->route($this->route . '.index')->with('success_message', 'Update Chapter Berhasil');
        }
        return redirect()->route($this->route . '.index')->with('error_message', 'Update Chapter Gagal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $updateChapter = $this->getChapter->findOrFail($id)->delete();
        if ($updateChapter) {
            return redirect()->route($this->route . '.index')->with('success_message', 'Delete Chapter Berhasil');
        }
        return redirect()->route($this->route . '.index')->with('error_message', 'Delete Chapter Gagal');
    }
}
