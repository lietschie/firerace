<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cool extends Model
{
    //
    public $table = 'cool';
    public $fillable = [
        'id',
        'name',
        'leader_id',
        'location',
        'total_point'
    ];

    public function pivotUser(){
        return $this->belongsToMany(User::class,'anggota_cool','cool_id','user_id');
    }

    // public function pivot

    public function hasManyScore(){
        return $this->hasMany(CoolScore::class,'cool_id');
    }


    public function leader(){
        return $this->belongsTo(User::class,'leader_id','id');
    }
}
