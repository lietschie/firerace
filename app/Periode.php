<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    //

    public $table = 'periode';

    public $fillable = [
        'name',
        'tanggal'
    ];
}
