<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoolScore extends Model
{
    //

    public $table = 'cool_score';

    public $fillable = [
        'periode_id',
        'cool_id',
        'parameter_id',
        'score_id',
        'score',
        'qty',
        'total_score'
    ];

    public function periode(){
        return $this->belongsTo(Periode::class,'periode_id');
    }
    public function parameter(){
        return $this->belongsTo(Parameter::class,'parameter_id');
    }
    public function cool(){
        return $this->belongsTo(Cool::class,'cool_id');
    }
}